# We use java 11 docker image as our base image
FROM openjdk:11-jdk

# Add our variables
# This is the version of Android we are going to compile with
ARG ANDROID_COMPILE_SDK=33

# The build tool version we need
ARG ANDROID_BUILD_TOOLS=32.0.0

# This is the version of the command line tool we are going to download from the official site
ARG ANDROID_SDK_TOOLS=8512546

ARG ANDROID_SDK_ROOT=${PWD}/android-home

# Installing some packages
RUN apt-get --quiet update --yes

RUN apt-get --quiet install --yes wget tar unzip lib32stdc++6 lib32z1

RUN apt-get --quiet install --yes libx11-dev libpulse0 libgl1 libnss3 libxcomposite-dev libxcursor1 libasound2-dev

# Creating environment variable for our working directory
RUN export ANDROID_SDK_ROOT="${PWD}/android-home"

# Creating the working directory
RUN mkdir $ANDROID_SDK_ROOT

# Downloading the Android SDK Tool and saving it in the working directory
RUN wget -q --output-document=$ANDROID_SDK_ROOT/cmdline-tools.zip https://dl.google.com/android/repository/commandlinetools-linux-${ANDROID_SDK_TOOLS}_latest.zip

# Switch to the working diretory
WORKDIR $ANDROID_SDK_ROOT

# The downloaded command line tool is a zip file, so we have to unzip it
RUN unzip cmdline-tools.zip -d cmdline-tools

# Remove the zip file since it will occupy space
RUN rm -f cmdline-tools.zip

# Move into the comdline-tool
WORKDIR cmdline-tools

# Inside the cmdline-tools directory there is another directory named cmdline-tool, rename it to tools
RUN mv cmdline-tools tools || true

WORKDIR ../

WORKDIR ../

# Set Android_home to our SDK location
ENV ANDROID_HOME=$ANDROID_SDK_ROOT

# Add our SDK bin to the global environment path
ENV PATH=$ANDROID_SDK_ROOT/cmdline-tools/tools/bin/:$PATH

# Checking if export of the path works by checking the SDK version
RUN sdkmanager --version

# Use yes to accept licenses
RUN yes | sdkmanager --licenses || true

WORKDIR $ANDROID_SDK_ROOT

RUN sdkmanager --install "platform-tools" "platforms;android-33" "build-tools;30.0.3"

# Set Volome to SDK location
VOLUME $ANDROID_SDK_ROOT

WORKDIR ../