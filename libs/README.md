1. Get the aar (Android Archive) file from [OpenStreetMap Maven Repository](https://repo1.maven.org/maven2/org/osmdroid/osmdroid-android/6.1.1/osmdroid-android-6.1.1.aar).
2. Extract the `osmdroid-android-6.1.1.aar` file.
3. Convert `.aar` to `.jar` by renaming `osmdroid-android-6.1.1.aar/classes.jar` to `osmdroid-android-6.1.1.jar`.
4. Copy `osmdroid-android-6.1.1.jar` into desired place (e.g `libs/`).