package com.example.hello;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Button;
import android.widget.Toast;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ActionBar; 

import com.example.hello.HomeFragment;
import com.example.hello.InputFragment;
import com.example.hello.MapsFragment;

public class MainActivity extends Activity {

    private FragmentManager fragmentManager;
    private ActionBar actionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Set the custom action bar view as the action bar
        actionBar = getActionBar();

        if (actionBar != null) {
            actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
            actionBar.setCustomView(R.layout.custom_action_bar);
        }

        fragmentManager = getFragmentManager();

        Button homeButton = findViewById(R.id.navigation_home); // Button adalah nama class atau nama tipe data, sedangkan homeButton adalah nama objek, dan findViewById
        Button inputButton = findViewById(R.id.navigation_input);
        Button mapsButton = findViewById(R.id.navigation_maps);

        displayFragment(new HomeFragment());
        
        homeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showToast("Home Button Clicked");
                displayFragment(new HomeFragment());
            }
        });

        inputButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showToast("Input Button Clicked");
                displayFragment(new InputFragment());
            }
        });

        mapsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showToast("Maps Button Clicked");
                displayFragment(new MapsFragment());
            }
        });
    }

    public void setNamaPengguna(String nama) {
        if (actionBar != null) {
            actionBar.setTitle(nama); // Set the title of the ActionBar
        }
    }

    private void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    private void displayFragment(Fragment fragment) {
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.fragment_container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
}
