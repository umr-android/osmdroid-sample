package com.example.hello;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.example.hello.MainActivity;

public class InputFragment extends Fragment {

    private EditText namaEditText;
    private EditText hobiEditText;
    private Button simpanButton;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_input, container, false);

        // Initialize UI elements
        namaEditText = view.findViewById(R.id.nama_edittext);
        hobiEditText = view.findViewById(R.id.hobi_edittext);
        simpanButton = view.findViewById(R.id.simpan_button);

        // Set a click listener for the "Simpan" button
        simpanButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Get the entered name and hobby
                String nama = namaEditText.getText().toString();
                String hobi = hobiEditText.getText().toString();

                // Update the name in the activity (to be displayed on every page)
                MainActivity activity = (MainActivity) getActivity();
                if (activity != null) {
                    activity.setNamaPengguna(nama);
                }
            }
        });

        return view;
    }
}
